/*
 * P2PFaaS - A framework for FaaS Load Balancing
 * Copyright (c) 2019 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package scheduler

import (
	"fmt"
	"scheduler/log"
	"scheduler/memdb"
	"scheduler/queue"
	"scheduler/scheduler_service"
	"scheduler/service_discovery"
	"scheduler/service_learning"
	"scheduler/types"
	"scheduler/utils"
	"strconv"
	"time"
)

const DoubleLearningSchedulerName = "DoubleLearningScheduler"

const DoubleLearningSchedulerHeaderKeyStateA = "X-P2pfaas-Scheduler-Learning-State"
const DoubleLearningSchedulerHeaderKeyActionA = "X-P2pfaas-Scheduler-Learning-Action"
const DoubleLearningSchedulerHeaderKeyEpsA = "X-P2pfaas-Scheduler-Learning-Eps"
const DoubleLearningSchedulerHeaderKeyEidA = "X-P2pfaas-Scheduler-Learning-Eid"

const DoubleLearningSchedulerHeaderKeyStateB = "X-P2pfaas-Scheduler-Learning-State-D"
const DoubleLearningSchedulerHeaderKeyActionB = "X-P2pfaas-Scheduler-Learning-Action-D"
const DoubleLearningSchedulerHeaderKeyEpsB = "X-P2pfaas-Scheduler-Learning-Eps-D"
const DoubleLearningSchedulerHeaderKeyEidB = "X-P2pfaas-Scheduler-Learning-Eid-D"

const DoubleLearningSchedulerHeaderKeyFps = "X-P2pfaas-Scheduler-Learning-Fps"
const DoubleLearningSchedulerHeaderKeyTaskType = "X-P2pfaas-Scheduler-Learning-Task-Type"

type DoubleLearningScheduler struct {
	NumberOfTaskTypes uint64 // number of task types
}

func (s DoubleLearningScheduler) GetFullName() string {
	return fmt.Sprintf("%s(%d)", DoubleLearningSchedulerName, s.NumberOfTaskTypes)
}

func (s DoubleLearningScheduler) GetScheduler() *types.SchedulerDescriptor {
	return &types.SchedulerDescriptor{
		Name: DoubleLearningSchedulerName,
		Parameters: []string{
			fmt.Sprintf("%d", s.NumberOfTaskTypes),
		},
	}
}

func (s DoubleLearningScheduler) Schedule(req *types.ServiceRequest) (*JobResult, error) {
	var err error
	var state []float64
	var totalLoad int64
	var actionRes *service_learning.EntryActOutput

	log.Log.Debugf("Scheduling job %s", req.ServiceName)
	now := time.Now()
	timingsStart := types.TimingsStart{ArrivedAt: &now}

	//get state
	state, totalLoad, err = s.getState(req)
	actEntry := service_learning.EntryAct{
		State: state,
	}

	// make decision: send request to learning service via socket communication
	actionRes, err = service_learning.SocketAct(&actEntry)
	if err != nil {
		return nil, CannotRetrieveAction{err}
	}

	return s.exeuteAction(req, actionRes, &timingsStart, state, totalLoad)
}

func (s DoubleLearningScheduler) getState(req *types.ServiceRequest) ([]float64, int64, error) {

	//declaring variables
	var err error
	var machineIp int
	taskType := float64(0)
	state := []float64{taskType}
	totalLoad := int64(0)

	//check if the request is external to add sender id to the state (double learning execution: second node)
	if req.External {
		machineIp, err = utils.ConvertIpStringToInt((*req.Headers)[utils.HttpHeaderP2PFaaSPeersSenderIp])
		state = append(state, float64(machineIp))
	}

	// parse the current task type
	taskTypeStr := (*req.Headers)[DoubleLearningSchedulerHeaderKeyTaskType]
	if taskTypeStr != "" {
		taskType, err = strconv.ParseFloat(taskTypeStr, 64)
		if err != nil {
			log.Log.Warningf("Cannot parse fps in headers: %s", taskTypeStr)
			taskType = 0
		}
	}
	log.Log.Debugf("Using taskType=%f", taskType)

	// update the service request task type
	req.ServiceType = int64(taskType)

	// prepare the state to be sent to the learner
	mapRunningFunctionsOfType := memdb.GetTotalRunningFunctionsOfType()
	mapQueueLengthOfType := queue.GetLengthOfTypes()
	statesMaps := []map[int64]int64{mapRunningFunctionsOfType, mapQueueLengthOfType}

	// state detailed queues
	for _, stateMap := range statesMaps {
		for i := 0; i < int(s.NumberOfTaskTypes); i++ {
			loadOfState, exists := stateMap[int64(i)]
			if !exists {
				state = append(state, float64(0))
				continue
			}
			totalLoad += loadOfState
			state = append(state, float64(loadOfState))
		}
	}

	return state, totalLoad, err
}

func (s DoubleLearningScheduler) exeuteAction(req *types.ServiceRequest,
	actionRes *service_learning.EntryActOutput,
	timingsStart *types.TimingsStart,
	state []float64,
	totalLoad int64) (*JobResult, error) {

	//declaring variables
	var jobResult *JobResult
	var err error
	var targetMachineIp string

	// actuate the action
	actionInt := int64(actionRes.Action)
	eps := actionRes.Eps

	if actionInt == 0 { // reject
		var result JobResult
		if req.External {
			result = JobResult{}
		} else {
			result = JobResult{TimingsStart: timingsStart, Scheduler: s.GetFullName()}
			timingsStart.ScheduledAt = utils.GetTimeNow()
		}
		s.addHeadersToResult(&result, req.Id, req.External, state, actionRes.Action, eps)

		return &result, JobDeliberatelyRejected{}
	}

	if actionInt == 1 || req.External { // execute locally
		timingsStart.ScheduledAt = utils.GetTimeNow()

		jobResult, err = executeJobLocally(req, timingsStart, s.GetFullName())
		s.addHeadersToResult(jobResult, req.Id, req.External, state, actionRes.Action, eps)

		return jobResult, err
	}

	if actionInt == 2 { // probe-and-forward
		// save time
		startedProbingTime := time.Now()
		timingsStart.ProbingStartedAt = &startedProbingTime
		// get N Random machines and ask them for mapRunningFunctionsOfType and pick the least loaded
		leastLoaded, _, err := scheduler_service.GetLeastLoadedMachineOfNRandom(1, uint(totalLoad), true, true)
		// save time
		endProbingTime := time.Now()
		timingsStart.ProbingEndedAt = &endProbingTime

		if err != nil {
			log.Log.Debugf("Error in retrieving machines %s", err.Error())
			// no machine less loaded than us, we are obliged to run the job in this machine or discard the job
			// if we cannot handle it
			jobResult, err = executeJobLocally(req, timingsStart, s.GetFullName())
			s.addHeadersToResult(jobResult, req.Id, req.External, state, actionRes.Action, eps)

			return jobResult, err
		}

		jobResult, err = executeJobExternally(req, leastLoaded, timingsStart, s.GetFullName())
		s.addHeadersToResult(jobResult, req.Id, req.External, state, actionRes.Action, eps)

		return jobResult, err
	}

	// otherwise forward
	targetMachineI := int64(actionRes.Action - 3)
	targetMachineIp, err = service_discovery.GetMachineIpAtIndex(targetMachineI, true)
	if err != nil {
		log.Log.Errorf("Cannot schedule job to machine i=%d of %d: %s", targetMachineI, service_discovery.GetCachedMachineNumber(), err)
		return nil, CannotRetrieveRecipientNode{err}
	}
	log.Log.Debugf("Forwarding to machine %s", targetMachineIp)

	jobResult, err = executeJobExternally(req, targetMachineIp, timingsStart, s.GetFullName())
	s.addHeadersToResult(jobResult, req.Id, req.External, state, actionRes.Action, eps)
	return jobResult, err
}

func (s DoubleLearningScheduler) addHeadersToResult(result *JobResult, reqId uint64, reqExternal bool, state []float64, action float64, eps float64) {
	if result != nil {
		if result.ResponseHeaders == nil {
			result.ResponseHeaders = &map[string]string{}
		}
		if reqExternal {
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyEidB] = fmt.Sprintf("%d", reqId)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyStateB] = utils.ArrayFloatToStringCommas(state)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyActionB] = fmt.Sprintf("%f", action)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyEpsB] = fmt.Sprintf("%f", eps)
		} else {
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyEidA] = fmt.Sprintf("%d", reqId)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyStateA] = utils.ArrayFloatToStringCommas(state)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyActionA] = fmt.Sprintf("%f", action)
			(*result.ResponseHeaders)[DoubleLearningSchedulerHeaderKeyEpsA] = fmt.Sprintf("%f", eps)
		}
	} else {
		log.Log.Errorf("result is nil, cannot add headers")
	}
}
